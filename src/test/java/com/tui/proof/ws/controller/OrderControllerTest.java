package com.tui.proof.ws.controller;

import com.tui.proof.dtos.OrderRequest;
import com.tui.proof.dtos.OrderResponse;
import com.tui.proof.model.Address;
import com.tui.proof.model.Client;
import io.r2dbc.spi.ConnectionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Base64;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderControllerTest {

    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private ConnectionFactory connectionFactory;
    private UUID orderId;
    private OrderRequest orderRequest;

    @BeforeEach
    public void setUp() {
        cleanupDatabase();
        orderId = UUID.randomUUID();
        orderRequest = getOrderRequest();
    }

    public void cleanupDatabase() {
        Mono<Void> cleanupTables = Mono.from(connectionFactory.create())
            .flatMapMany(connection -> {
                String[] statements = {
                    "DELETE FROM orders",
                    "DELETE FROM clients",
                    "DELETE FROM addresses"
                };
                return Flux.fromArray(statements)
                    .flatMap(statement ->
                        connection.createStatement(statement).execute())
                            .thenMany(connection.close());
            })
            .then();

        StepVerifier.create(cleanupTables).verifyComplete();
    }

    @Test
    @WithMockUser
    void createOrder() {
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().isOk();
    }

    @Test
    @WithMockUser
    void createOrderWithInvalidEmail() {
        Client client = orderRequest.getClient();
        client.setEmail("invalidEmail");
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyEmail() {
        Client client = orderRequest.getClient();
        client.setEmail("");
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithInvalidPhone() {
        Client client = orderRequest.getClient();
        client.setTelephone("00000000");
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyPhone() {
        Client client = orderRequest.getClient();
        client.setTelephone("");
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyName() {
        Client client = orderRequest.getClient();
        client.setFirstName("");
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithShortName() {
        Client client = orderRequest.getClient();
        client.setFirstName("x");
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithoutPassword() {
        Client client = orderRequest.getClient();
        client.setPassword(null);
        orderRequest.setClient(client);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithInvalidNumberOfPilotes() {
        orderRequest.setPilotes(2);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyClient() {
        orderRequest.setClient(null);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyCity() {
        Address address = orderRequest.getDeliveryAddress();
        address.setCity("");
        orderRequest.setDeliveryAddress(address);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyCountry() {
        Address address = orderRequest.getDeliveryAddress();
        address.setCountry("");
        orderRequest.setDeliveryAddress(address);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyPostcode() {
        Address address = orderRequest.getDeliveryAddress();
        address.setPostcode("");
        orderRequest.setDeliveryAddress(address);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithEmptyStreet() {
        Address address = orderRequest.getDeliveryAddress();
        address.setStreet("");
        orderRequest.setDeliveryAddress(address);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void createOrderWithInvalidPostcode() {
        Address address = orderRequest.getDeliveryAddress();
        address.setPostcode("1234g5");
        orderRequest.setDeliveryAddress(address);
        webTestClient.post()
            .uri("/api/v1/orders").contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().is4xxClientError();
    }

    @Test
    @WithMockUser
    void updateOrder() {
        webTestClient.put()
            .uri("/api/v1/orders/" + orderId).contentType(MediaType.APPLICATION_JSON)
            .bodyValue(orderRequest)
            .exchange()
            .expectStatus().isOk();
    }

    @Test
    void searchOrders() {
        createOrder();
        String credentials = getClient().getEmail() + ":" + getClient().getPassword();
        String authorizationHeader = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());
        webTestClient.get()
            .uri(uriBuilder -> uriBuilder
                .path("/api/v1/orders/get")
                .queryParam("keyword", "ame")
                .build())
            .header("Authorization", authorizationHeader)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(OrderResponse.class)
            .value(orderResponses -> {
                assertThat(orderResponses).isNotEmpty();
            });
    }

    @Test
    void searchOrdersWithoutExistKeywordInOrders() {
        createOrder();
        String credentials = getClient().getEmail() + ":" + getClient().getPassword();
        String authorizationHeader = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());
        webTestClient.get()
            .uri(uriBuilder -> uriBuilder
                .path("/api/v1/orders/get")
                .queryParam("keyword", "ghy")
                .build())
            .header("Authorization", authorizationHeader)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(OrderResponse.class)
            .value(orderResponses -> {
                assertThat(orderResponses).isEmpty();
            });
    }

    @Test
    void searchOrdersWithBadCredentials() {
        createOrder();
        String credentials = "test" + ":" + getClient().getPassword();
        String authorizationHeader = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());
        webTestClient.get()
            .uri(uriBuilder -> uriBuilder
                .path("/api/v1/orders/get")
                .queryParam("keyword", "ame")
                .build())
            .header("Authorization", authorizationHeader)
            .exchange()
            .expectStatus().isUnauthorized();
    }

    @Test
    void findOrderById() {
        webTestClient.get()
            .uri("/api/v1/orders/get/" + orderId)
            .exchange()
            .expectStatus().isOk();
    }

    @Test
    void findOrderByIdWithInvalidId() {
        webTestClient.get()
            .uri("/api/v1/orders/get/" + "ecrvtybuij2n8iuwdne")
            .exchange()
            .expectStatus().is5xxServerError();
    }

    @Test
    void deleteOrder() {
        webTestClient.delete()
            .uri("/api/v1/orders/" + orderId)
            .exchange()
            .expectStatus().isOk();
    }

    @Test
    void deleteOrderWithInvalidId() {
        webTestClient.delete()
            .uri("/api/v1/orders/" + "wdfg2yuinfdok")
            .exchange()
            .expectStatus().is5xxServerError();
    }

    private OrderRequest getOrderRequest() {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setClient(getClient());
        orderRequest.setPilotes(5);
        orderRequest.setDeliveryAddress(getAddress());
        return orderRequest;
    }

    private static Client getClient() {
        Client client = new Client();
        client.setFirstName("firstName");
        client.setLastName("lastName");
        client.setEmail("email@email.com");
        client.setTelephone("+380995672855");
        client.setPassword("testPassword");
        return client;
    }

    private static Address getAddress() {
        Address address = new Address();
        address.setCity("City");
        address.setCountry("Country");
        address.setStreet("Street");
        address.setPostcode("12345");
        return address;
    }
}