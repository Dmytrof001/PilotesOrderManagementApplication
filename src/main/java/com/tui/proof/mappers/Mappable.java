package com.tui.proof.mappers;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface Mappable<E, D> {
    Mono<D> toDto(Mono<E> entity);

    D toDto(E entity);

    Flux<D> toDtos(Flux<E> entities);

    List<D> toDtos(List<E> entities);

    Mono<E> toEntity(Mono<D> dto);

    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);
}
