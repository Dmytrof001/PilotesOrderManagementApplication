package com.tui.proof.mappers;

import com.tui.proof.dtos.OrderResponse;
import com.tui.proof.model.Client;
import com.tui.proof.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderResponseMapper extends Mappable<Order, OrderResponse> {
    @Override
    default Mono<OrderResponse> toDto(Mono<Order> entity) {
        return entity.map(order -> {
            OrderResponse orderResponse = new OrderResponse();
            orderResponse.setId(order.getId());
            orderResponse.setClient(deletePasswordInResponse(order.getClient()));
            orderResponse.setDeliveryAddress(order.getDeliveryAddress());
            orderResponse.setPilotes(order.getPilotes());
            orderResponse.setOrderTotal(order.getOrderTotal());
            orderResponse.setCreatedAt(order.getCreatedAt());
            return orderResponse;
        });
    }

    @Override
    default Mono<Order> toEntity(Mono<OrderResponse> dto) {
        return dto.map(orderResponse -> {
            Order order = new Order();
            order.setId(orderResponse.getId());
            order.setClient(deletePasswordInResponse(order.getClient()));
            order.setDeliveryAddress(orderResponse.getDeliveryAddress());
            order.setPilotes(orderResponse.getPilotes());
            order.setOrderTotal(orderResponse.getOrderTotal());
            order.setCreatedAt(orderResponse.getCreatedAt());
            return order;
        });
    }

    @Override
    default Flux<OrderResponse> toDtos(Flux<Order> entities) {
        return entities.map(order -> {
            OrderResponse orderResponse = new OrderResponse();
            orderResponse.setId(order.getId());
            orderResponse.setClient(deletePasswordInResponse(order.getClient()));
            orderResponse.setDeliveryAddress(order.getDeliveryAddress());
            orderResponse.setPilotes(order.getPilotes());
            orderResponse.setOrderTotal(order.getOrderTotal());
            orderResponse.setCreatedAt(order.getCreatedAt());
            return orderResponse;
        });
    }

    private Client deletePasswordInResponse(Client client) {
        client.setPassword(null);
        return client;
    }
}
