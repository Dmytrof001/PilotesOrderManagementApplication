package com.tui.proof.mappers;

import com.tui.proof.dtos.OrderRequest;
import com.tui.proof.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrderRequestMapper extends Mappable<Order, OrderRequest> {

    @Override
    default Mono<OrderRequest> toDto(Mono<Order> entity) {
        return entity.map(order -> {
            OrderRequest orderRequest = new OrderRequest();
            orderRequest.setClient(order.getClient());
            orderRequest.setDeliveryAddress(order.getDeliveryAddress());
            orderRequest.setPilotes(order.getPilotes());
            return orderRequest;
        });
    }

    @Override
    default Mono<Order> toEntity(Mono<OrderRequest> dto) {
        return dto.map(orderRequest -> {
            Order order = new Order();
            order.setClient(orderRequest.getClient());
            order.setDeliveryAddress(orderRequest.getDeliveryAddress());
            order.setPilotes(orderRequest.getPilotes());
            return order;
        });
    }

    @Override
    default Flux<OrderRequest> toDtos(Flux<Order> entities) {
        return entities.map(order -> {
            OrderRequest orderRequest = new OrderRequest();
            orderRequest.setClient(order.getClient());
            orderRequest.setDeliveryAddress(order.getDeliveryAddress());
            orderRequest.setPilotes(order.getPilotes());
            return orderRequest;
        });
    }
}
