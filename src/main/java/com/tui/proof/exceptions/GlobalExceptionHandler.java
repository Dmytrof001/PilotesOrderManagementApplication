package com.tui.proof.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(WebExchangeBindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Mono<String> handleValidationException(WebExchangeBindException ex) {
        return Mono.just(ex.getMessage());
    }

    @ExceptionHandler({OrderUpdateTimeoutException.class, IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Mono<Object> handleInvalidDataException(
        final RuntimeException ex
    ) {
        return Mono.just(ex.getMessage());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Mono<Object> handleUserNotFoundException(
        final BadCredentialsException ex
    ) {
        return Mono.just(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Mono<Object> handleOtherExceptions(
        final Exception ex
    ) {
        return Mono.just(ex.getMessage());
    }
}

