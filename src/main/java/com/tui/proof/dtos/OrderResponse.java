package com.tui.proof.dtos;

import com.tui.proof.model.Address;
import com.tui.proof.model.Client;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@ToString
public class OrderResponse {

    private UUID id;
    private Client client;
    private Address deliveryAddress;
    private int pilotes;
    private double orderTotal;
    private LocalDateTime createdAt;
}
