package com.tui.proof.dtos;

import com.tui.proof.model.Address;
import com.tui.proof.model.Client;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderRequest {

    @Valid
    @NotNull
    private Client client;
    @Valid
    @NotNull
    private Address deliveryAddress;
    private Integer pilotes;
}
