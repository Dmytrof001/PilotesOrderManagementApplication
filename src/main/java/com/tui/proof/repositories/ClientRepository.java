package com.tui.proof.repositories;

import com.tui.proof.model.Client;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface ClientRepository extends ReactiveCrudRepository<Client, UUID> {
    Mono<Client> findClientByEmail(String email);
}
