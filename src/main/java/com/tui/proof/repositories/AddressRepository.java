package com.tui.proof.repositories;

import com.tui.proof.model.Address;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AddressRepository extends ReactiveCrudRepository<Address, UUID> {
}
