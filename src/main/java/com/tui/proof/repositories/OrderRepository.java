package com.tui.proof.repositories;

import com.tui.proof.model.Order;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface OrderRepository extends ReactiveCrudRepository<Order, UUID> {
    @Query("SELECT o.* FROM orders o INNER JOIN clients c ON o.client_id = c.id WHERE o.client_id = :userId AND (c.first_name LIKE CONCAT('%', :keyword, '%') OR c.last_name LIKE CONCAT('%', :keyword, '%'))")
    Flux<Order> findByUserIdAndClientNameKeyword(@Param("userId") UUID userId, @Param("keyword") String keyword);
}

