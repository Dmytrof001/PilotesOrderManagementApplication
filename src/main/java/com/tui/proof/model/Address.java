package com.tui.proof.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;


@Data
@Table("addresses")
@ToString
public class Address implements Persistable<UUID> {

    @JsonIgnore
    @Id
    private UUID id;
    @NotBlank(message = "Street cannot be blank")
    @Size(max = 255, message = "Street length must be less than or equal to 255 characters")
    private String street;

    @NotBlank(message = "Postcode cannot be blank")
    @Pattern(regexp = "\\d{5}", message = "Postcode must consist of 5 digits")
    private String postcode;

    @NotBlank(message = "City cannot be blank")
    @Size(max = 250, message = "City length must be less than or equal to 250 characters")
    private String city;

    @NotBlank(message = "Country cannot be blank")
    @Size(max = 250, message = "Country length must be less than or equal to 250 characters")
    private String country;
    @Transient
    @JsonIgnore
    private boolean newAddress;

    @Override
    public boolean isNew() {
        return this.newAddress || id == null;
    }

    public Address setAsNew() {
        this.newAddress = true;
        return this;
    }
}
