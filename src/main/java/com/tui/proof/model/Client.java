package com.tui.proof.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@Table("clients")
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Client implements Persistable<UUID> {

  @JsonIgnore
  @Id
  private UUID id;
  @NotBlank(message = "First name are required")
  @Size(min = 2, message = "user name should have at least 2 characters")
  private String firstName;
  @Size(min = 2, message = "user name should have at least 2 characters")
  @NotBlank(message = "Last name are required")
  private String lastName;
  @NotBlank
  @Email(message = "Email not valid")
  private String email;
  @Pattern(regexp = "^([+]{1})[0-9]{8,17}$", message = "Phone number not valid")
  private String telephone;
  @NotBlank
  private String password;
  @Transient
  @JsonIgnore
  private boolean newClient;

  @Override
  public boolean isNew() {
    return this.newClient || id == null;
  }

  public Client setAsNew() {
    this.newClient = true;
    return this;
  }
}
