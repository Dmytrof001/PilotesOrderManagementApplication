package com.tui.proof.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Table("orders")
@ToString
public class Order implements Persistable<UUID> {

  @Id
  private UUID id;
  @Transient
  private Client client;
  @Column("client_id")
  @JsonIgnore
  private UUID clientId;
  @Transient
  private Address deliveryAddress;
  @Column("delivery_address_id")
  @JsonIgnore
  private UUID deliveryAddressId;
  @NotNull(message = "Pilotes must be specified")
  private int pilotes;
  private double orderTotal;
  private LocalDateTime createdAt;
  @Transient
  @JsonIgnore
  private boolean newOrder;

  @Override
  public boolean isNew() {
    return this.newOrder || id == null;
  }

  public Order setAsNew() {
    this.newOrder = true;
    return this;
  }
}
