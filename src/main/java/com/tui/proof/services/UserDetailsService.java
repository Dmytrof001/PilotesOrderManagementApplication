package com.tui.proof.services;

import com.tui.proof.repositories.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserDetailsService implements ReactiveUserDetailsService {
    private final ClientRepository clientRepository;

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return clientRepository.findClientByEmail(username)
            .map(client -> User.withUsername(client.getEmail())
                .password(client.getPassword())
                .roles("USER")
                .build());
    }
}
