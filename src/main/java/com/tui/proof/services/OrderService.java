package com.tui.proof.services;

import com.tui.proof.exceptions.OrderUpdateTimeoutException;
import com.tui.proof.model.Address;
import com.tui.proof.model.Client;
import com.tui.proof.model.Order;
import com.tui.proof.repositories.AddressRepository;
import com.tui.proof.repositories.ClientRepository;
import com.tui.proof.repositories.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final ClientRepository clientRepository;
    private final AddressRepository addressRepository;
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final PasswordEncoder passwordEncoder;
    private static final double PRICE_OF_PILOTES = 1.33;
    private static final int ORDER_UPDATE_DEADLINE = 5;


    @SneakyThrows
    public Mono<Order> createOrder(Order order) {
        checkNumberOfPilotes(order.getPilotes());
        setOrder(order);
        return clientRepository.save(getClient(order))
            .zipWith(addressRepository.save(getAddress(order)))
            .flatMap(tuple -> {
                order.setAsNew();
                order.setClientId(tuple.getT1().getId());
                order.setDeliveryAddressId(tuple.getT2().getId());
                return orderRepository.save(order)
                    .doOnSuccess(savedOrder -> kafkaTemplate.send("orders", savedOrder.toString()));
            });
    }

    public Mono<Order> findOrderById(UUID id) {
        return orderRepository.findById(id);
    }

    public Mono<Void> cancelOrder(UUID id) {
        return orderRepository.deleteById(id);
    }

    private void checkNumberOfPilotes(int pilotes) {
        if (!Arrays.asList(5, 10, 15).contains(pilotes)) {
            throw new IllegalArgumentException("Number of pilotes must be 5, 10, or 15");
        }
    }

    private void setOrder(Order order) {
        order.setOrderTotal(order.getPilotes() * PRICE_OF_PILOTES);
        order.setCreatedAt(LocalDateTime.now());
        order.setId(UUID.randomUUID());
    }

    private Address getAddress(Order order) {
        Address deliveryAddress = order.getDeliveryAddress();
        deliveryAddress.setId(UUID.randomUUID());
        deliveryAddress.setAsNew();
        return deliveryAddress;
    }

    private Client getClient(Order order) {
        Client client = order.getClient();
        client.setPassword(passwordEncoder.encode(client.getPassword()));
        client.setId(UUID.randomUUID());
        client.setAsNew();
        return client;
    }

    public Mono<Order> updateOrder(UUID orderId, Order updatedOrder) {
        return orderRepository.findById(orderId)
            .flatMap(existingOrder -> {
                if (ChronoUnit.MINUTES.between(existingOrder.getCreatedAt(), LocalDateTime.now()) > ORDER_UPDATE_DEADLINE) {
                    return Mono.error(new OrderUpdateTimeoutException("Order is too old to update."));
                }
                Mono<Address> addressMono = updateAddress(updatedOrder, existingOrder);
                Mono<Client> clientMono = updateClient(updatedOrder, existingOrder);
                return Mono.zip(clientMono, addressMono)
                    .flatMap(tuple -> {
                        existingOrder.setClient(tuple.getT1());
                        existingOrder.setDeliveryAddress(tuple.getT2());
                        return orderRepository.save(existingOrder);
                    });
            });
    }

    private Mono<Client> updateClient(Order updatedOrder, Order existingOrder) {
        Client updatedClient = updatedOrder.getClient();
        return clientRepository.findById(existingOrder.getClientId())
            .flatMap(existingClient -> {
                existingClient.setFirstName(updatedClient.getFirstName());
                existingClient.setLastName(updatedClient.getLastName());
                existingClient.setEmail(updatedClient.getEmail());
                existingClient.setTelephone(updatedClient.getTelephone());
                return clientRepository.save(existingClient);
            });
    }

    private Mono<Address> updateAddress(Order updatedOrder, Order existingOrder) {
        Address updatedAddress = updatedOrder.getDeliveryAddress();
        return addressRepository.findById(existingOrder.getDeliveryAddressId())
            .flatMap(existingAddress -> {
                existingAddress.setStreet(updatedAddress.getStreet());
                existingAddress.setCity(updatedAddress.getCity());
                existingAddress.setPostcode(updatedAddress.getPostcode());
                return addressRepository.save(existingAddress);
            });
    }

    public Flux<Order> searchOrdersByUserIdAndKeyword(String username, String keyword) {
        return clientRepository.findClientByEmail(username)
            .flatMapMany(client -> {
                Flux<Order> orders = orderRepository.findByUserIdAndClientNameKeyword(client.getId(), keyword);
                return orders.flatMap(this::populateOrderWithClientAndAddress);
            });
    }

    private Mono<Order> populateOrderWithClientAndAddress(Order order) {
        Mono<Client> clientMono = clientRepository.findById(order.getClientId());
        Mono<Address> addressMono = addressRepository.findById(order.getDeliveryAddressId());
        return Mono.zip(clientMono, addressMono)
            .map(tuple -> {
                order.setClient(tuple.getT1());
                order.setDeliveryAddress(tuple.getT2());
                return order;
            });
    }
}

