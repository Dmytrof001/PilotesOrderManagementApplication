package com.tui.proof.ws.controller;

import com.tui.proof.dtos.OrderRequest;
import com.tui.proof.dtos.OrderResponse;
import com.tui.proof.mappers.OrderRequestMapper;
import com.tui.proof.mappers.OrderResponseMapper;
import com.tui.proof.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@RequestMapping("api/v1/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final OrderResponseMapper orderResponseMapper;
    private final OrderRequestMapper orderRequestMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Mono<OrderResponse> createOrder(@Valid @RequestBody OrderRequest order) {
        return orderResponseMapper.toDto(orderService.createOrder(orderRequestMapper.toEntity(order)));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<OrderResponse> updateOrder(@PathVariable UUID id, @Valid @RequestBody OrderRequest order) {
        return orderResponseMapper.toDto(orderService.updateOrder(id, orderRequestMapper.toEntity(order)));
    }

    @GetMapping("/get")
    @ResponseStatus(HttpStatus.OK)
    public Flux<OrderResponse> searchOrders(@RequestParam String keyword, @AuthenticationPrincipal UserDetails userDetails) {
        if (userDetails != null) {
            String username = userDetails.getUsername();
            return orderResponseMapper.toDtos(orderService.searchOrdersByUserIdAndKeyword(username, keyword));
        }  else {
            throw new BadCredentialsException("User details not found or invalid credentials.");
        }
    }

    @GetMapping("/get/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<OrderResponse> getOrder(@PathVariable UUID id) {
        return orderResponseMapper.toDto(orderService.findOrderById(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> deleteOrder(@PathVariable UUID id) {
        return orderService.cancelOrder(id);
    }
}

