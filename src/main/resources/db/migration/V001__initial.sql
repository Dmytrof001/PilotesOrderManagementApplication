CREATE TABLE IF NOT EXISTS addresses (
         id          UUID PRIMARY KEY,
         street      VARCHAR(255) NOT NULL,
         postcode    VARCHAR(10)  NOT NULL,
         city        VARCHAR(250) NOT NULL,
         country     VARCHAR(250) NOT NULL
);

CREATE TABLE IF NOT EXISTS clients (
       id           UUID PRIMARY KEY,
       first_name   VARCHAR(255) NOT NULL,
       last_name    VARCHAR(255) NOT NULL,
       email        VARCHAR(255) NOT NULL UNIQUE,
       telephone    VARCHAR(50)  NOT NULL UNIQUE,
       password     TEXT         NOT NULL
);

CREATE TABLE IF NOT EXISTS orders (
      id                  UUID   PRIMARY KEY,
      client_id           UUID   NOT NULL,
      delivery_address_id UUID   NOT NULL,
      pilotes             INT    NOT NULL,
      order_total         DOUBLE PRECISION NOT NULL,
      created_at          TIMESTAMP NOT NULL,
      FOREIGN KEY (client_id) REFERENCES clients(id),
      FOREIGN KEY (delivery_address_id) REFERENCES addresses(id)
);
